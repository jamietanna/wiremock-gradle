# Building Wiremock Standalone JARs with Gradle

A sample project for the blog post [Packaging Wiremock Extensions into the Standalone Server Runner](https://www.jvt.me/posts/2021/09/12/gradle-wiremock-extensions-standalone/).
